from numpy import array
from math import sqrt
import numpy as np

A = array([
    [0.45, 0.03, -0.01, 0.02, -0.111],
    [0.02, 0.375, -0.01, -0.01, 0],
    [0, 0.07, 0.44, 0, 0.113],
    [-0.03, 0.015, -0.02, 0.41, -0.084],
    [0.02, 0.01, 0, 0, 0.29]
])

b = array([-0.023, -0.690, 0.199, -1.952, 0.870])
eps = 0.1
k = 0
variables = array(['x', 'y', 'z', 'u', 'v'])


def yakobi(A, b, eps):
    n = len(A)
    x = [.0 for i in range(n)]
    i = 0
    converge = False
    while not converge:
        x_new = np.copy(x)
        for i in range(n):
            s1 = sum(A[i][j] * x_new[j] for j in range(i))
            s2 = sum(A[i][j] * x[j] for j in range(i + 1, n))
            x_new[i] = (b[i] - s1 - s2) / A[i][i]

        converge = sqrt(sum((x_new[i] - x[i]) ** 2 for i in range(n))) <= eps
        i += 1
        x = x_new
    # return x
    print(variables)
    for xi in x:
        print('%.5f' % xi, end=' ')

    print('\n')
    print('Кількість ітерацій: ', i)


yakobi(A, b, eps)
